// DSA Assignment.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "BST.h"
#include <iostream>
#include <fstream>
#include "DSA Assignment.h"
#include <string>
#include <sstream>
#include <vector>
using namespace std;

//Team Members
//S10189886D,Dicco Neoh Hyu Hang
//S10184437J,Mohan Saravanan
//S10189566E,Lee Thing Tsin, John

int main()
{
	int options = 0;
	string linename;
	ifstream infile;
	ofstream outfile;
	string line, stationName, stationNumber;
	
	do
	{
		cout << "1.Display All Station" << endl;
		cout << "2.Search a Station" << endl;
		cout << "3.Insert a new station" << endl;
		cout << "4.Find and display a route and its price, given the source and destination stations" << endl;
		cout << "5.Add new line" << endl;
		cout << "6.Find the shortest route and its price, given source and destination stations" << endl;
		cout << "7.Find up to 3 routes with price and distance, given source and destination names" << endl;
		cout << "Enter Options: ";
		cin >> options;

		switch (options)
		{
			//Dicco
			//This function is to provide the user to display all the updated station
		case 1:
			infile.open("Full/Stations.csv");
			if (!infile.is_open())
				cout << "File not found" << endl;
			while (infile.good())
			{
				getline(infile, stationNumber, ',');
				getline(infile, stationName, '\n');
			
			cout << stationNumber << " " << stationName << '\n';
			}
			break;

			//Sara
			//Search for Station using Binary Search Tree
		case 2:
			string user_input;
			BST station_BST = BST();
			cout << "Station Number: ";
			cin >> user_input;

			ifstream ip("Simple/Stations.csv");

			if (!ip.is_open())
				cerr << "File not found" << endl;

			while (ip.good()) {
				getline(ip, stationNumber, ',');
				getline(ip, stationName, '\n');
				
				station_BST.add(stationNumber, stationName);
			}
			cout << endl;
			cout << "Station Name: " << station_BST.search(user_input)->value << endl;

			break;

		//This function is allow users to add new line for the MRT
		//case 5:
		//	string newLine;
		//	cout << "Please type in which line do you want to add"<<endl;
		//	cout << "Example: AB1" << endl;
		//	cin >> newLine;
		//	outfile.open("Simple/Stations",std::ios_base::app);
		//	outfile << newLine<<endl;
		//	break;
			

		}

	} while (options != 7);
		


	}

	// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
	// Debug program: F5 or Debug > Start Debugging menu

	// Tips for Getting Started: 
	//   1. Use the Solution Explorer window to add/manage files
	//   2. Use the Team Explorer window to connect to source control
	//   3. Use the Output window to see build output and other messages
	//   4. Use the Error List window to view errors
	//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
	//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

