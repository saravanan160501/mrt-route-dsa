#pragma once

using namespace std;
typedef string Key;
#include<string>

struct Node {
	Key key;
	string value;
	// left tree pointer
	Node* left;
	// right tree pointer
	Node* right;  
};