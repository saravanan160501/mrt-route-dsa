#include "pch.h"
#include "BST.h"


BST::BST()
{
	root = NULL;
}

Node* BST::search(Key target)
{
	return search(root, target);
}

Node* BST::search(Node* t, Key target)
{
	// if value is nil
	if (t == NULL)	
		return NULL;
	else
	{
		// value is found
		if (t->key == target)	
			return t;
		else
			// left subtree search
			if (target < t->key)	
				return search(t->left, target);
		// right subtree search
			else 
				return search(t->right, target);
	}
}

void BST::add(Key key, string value)
{
	add(root, key, value);
}

void BST::add(Node*& t, Key key, string value)
{
	if (t == NULL)
	{
		Node* node = new Node;
		node->key = key;
		node->value = value;
		node->left = NULL;
		node->right = NULL;
		t = node;
	}
	else
		if (key < t->key)
			add(t->left, key, value);  // add to left subtree
		else
			add(t->right, key, value); // add to right subtree
}
