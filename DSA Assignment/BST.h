#pragma once

using namespace std;

#include<iostream>
#include "Node.h"


class BST
{
private:
	Node* root;

public:
	BST();

	Node* search(Key target);
	Node* search(Node* root, Key target);

	void add(Key key, string value);
	void add(Node*& root, Key key, string value);
};
